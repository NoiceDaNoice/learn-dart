const pi = 3.14;

//final & const

void main(){
  var a = const constClass(number: 5);
  var b = const constClass(number: 5);

  print(identical(a, b));
  print(pi);
}

class regularClass{
  final int number;
  static const myConst = 12;
  regularClass({this.number}){
    const anotherConst = 14;

    print(anotherConst);
  }
}

class constClass{
  final int number;

  const constClass({this.number});
}