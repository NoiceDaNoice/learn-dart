
//var & dynamic data type

void main(List<String> arguments) {
  // dynamic myDynamic;
  //
  // myDynamic = 'hello';
  // myDynamic = 12;
  // myDynamic = Person();
  //
  // if(myDynamic is Person){
  //   print(myDynamic.name);
  // }

  var myVar = Person();
  // myVar = 12;
  // myVar = 'hello';
  print(myVar.name);
}

class Person{
  String name = 'no name';
}