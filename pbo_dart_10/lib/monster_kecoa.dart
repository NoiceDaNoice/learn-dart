import 'package:pbo_dart_10/flying_monster.dart';
import 'package:pbo_dart_10/monster.dart';

class MonsterKecoa extends Monster implements FlyingMonster{
  @override
  String fly()=>"Syuung..";

  @override
  String move() {
    return "Jalan jalan santay";
  }
}