
import 'package:pbo_dart_10/flying_monster.dart';
import 'package:pbo_dart_10/monster_ubur_ubur.dart';

class MonsterUcoa extends MonsterUburUbur implements FlyingMonster{
  @override
  String fly() {
    return "terbang terbang melayang";
  }

}