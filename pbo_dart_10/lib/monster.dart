import 'package:pbo_dart_10/character.dart';

abstract class Monster extends Character {
  String eatHuman() => "Grr.. Delicious..Yummy..";
  String move();
}