
//generic

import 'package:pbo_dart_16/secure_box.dart';

void main(List<String> arguments) {
 var box = SecureBox<Person>(Person("Dodi"), "123");
 print(box.getData("123").name);
}

class Person {
  final String name;

  Person(this.name);
}