
//enum & cascade

void main(List<String> arguments) {
  MonsterUcoa m = MonsterUcoa(status: UcoaStatus.confused);
  print('hello world');
  m
    ..move()
    ..eat();


}

enum UcoaStatus{normal,poisoned,confused}

class MonsterUcoa{
  final UcoaStatus status;

  MonsterUcoa({this.status=UcoaStatus.normal});

  void move(){
    switch(status){
      case UcoaStatus.normal:
        print('ucoa is moving');
        break;
      case UcoaStatus.poisoned:
        print('ucoa cannot move. ucos is dying. ucoa need help');
        break;
      case UcoaStatus.confused:
        print('ucoa spinning. dart language is confusing');
        break;
      default:
    }
  }

  void eat(){
    print('ucoa eat indomie');
  }

}
