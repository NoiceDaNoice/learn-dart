import 'package:pbo_dart_09/character.dart';

abstract class Monster extends Character{
  String eatHuman() => "Grr.. Delicious..Yummy..";
  String move();
}