import 'dart:io';

import 'package:pbo_dart_09/flying_monster.dart';
import 'package:pbo_dart_09/hero.dart';
import 'package:pbo_dart_09/monster.dart';
import 'package:pbo_dart_09/monster_kecoa.dart';
import 'package:pbo_dart_09/monster_ubur_ubur.dart';
import 'package:pbo_dart_09/monster_ucoaa.dart';

//abstract class dan interface

void main(List<String> arguments) async{
  List<Monster> monsters = [];

  monsters.add(MonsterUburUbur());
  monsters.add(MonsterKecoa());
  monsters.add(MonsterUcoa());

  for(Monster m in monsters){
    if(m is FlyingMonster){
      print((m as FlyingMonster).fly());
    }
  }
}