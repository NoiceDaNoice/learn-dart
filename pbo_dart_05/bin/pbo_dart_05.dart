import 'dart:io';

//list

void main(List<String> arguments) {
  List<int> mylist = [1,1,6,4,3,6,2];
  List<String> list = [];
  
  // list[0] = 10 ;
  // int e = list[0];
  // print(e);

  // for(int i = 0; i < list.length ; i++){
  //   print(list[i]);
  // }

  // for(int bilangan in list){
  //   print(bilangan);
  // }

  // list.forEach((bilangan) {
  //   print(bilangan);
  // });

  // mylist.add(10);
  // mylist.addAll(list);
  // mylist.insert(1, 20);
  // mylist.insertAll(3, [30,40,20]);
  // mylist.remove(20);
  // mylist.removeLast();
  // mylist.removeAt(10);
  // mylist.removeRange(1, 4);


  // if(mylist.contains(6)){
  //   print("betul");
  // }

  // list = mylist.sublist(3,6);
  //
  // list.clear();

  // mylist.sort((a,b)=>b-a);

  // mylist.removeWhere((number) => number % 2 == 0);
  // if(mylist.every((number) => number % 2 != 0)){
  //   print("ganjil semua");
  // }else{
  //   print("tidak semua");
  // }

  // if(mylist.isNotEmpty){
  //   print("tidak kosong");
  // }

  // Set<int> s;
  // s = mylist.toSet();
  //
  // s.forEach((bilangan) {
  //   print(bilangan);
  // });

  // mylist.forEach((bilangan){
  //   list.add("angka " + bilangan.toString());
  // });

  list = mylist.map((number) => "angka " + number.toString()).toList();
  list.forEach((str) {
    print(str);
  });
}
