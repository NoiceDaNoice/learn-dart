import 'dart:io';

import 'package:pbo_dart_08/hero.dart';
import 'package:pbo_dart_08/monster.dart';
import 'package:pbo_dart_08/monster_kecoa.dart';
import 'package:pbo_dart_08/monster_ubur_ubur.dart';

//inheritance + as/is

void main(List<String> arguments) async{
  Hero h = Hero();
  Monster m = MonsterUburUbur();
  MonsterUburUbur u = MonsterUburUbur();
  List<Monster> monsters = [];

  monsters.add(MonsterUburUbur());
  monsters.add(MonsterKecoa());
  monsters.add(MonsterUburUbur());

  // h.healthPoint = -10;
  // m.healthPoint = 10;

  // print("Hero hp: " +h.healthPoint.toString());
  // print("Monster hp: " +m.healthPoint.toString());
  //
  // print(h.killAMonster());
  // print(m.eatHuman());
  // print(u.swim());

  print((m as MonsterUburUbur).swim());
  // for(Monster m in monsters){
  //   if(m is MonsterUburUbur){
  //     print(m.eatHuman());
  //     print(m.swim());
  //   }
  // }
}
