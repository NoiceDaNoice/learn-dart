import 'package:meta/meta.dart';

//Required, Assert, Try Catch

void main(List<String> arguments) {
  Person p;

  try{
    p = Person(name: null);
  }catch(e){
    print(e);
  }

  print('hello');
}

class Person{
  final String name;
  final int age;

  Person({@required this.name, this.age=0}){
   assert(name!=null,"you must give the person name");
  }


}
